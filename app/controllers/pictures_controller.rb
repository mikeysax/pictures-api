class PicturesController < ApplicationController
  require "http"

  def index
    url = params[:search] ? "#{base_url}&q=#{params[:search]}" : "#{base_url}&editors_choice=true"
    response = HTTP.get(url).body
    render json: JSON.parse(response)
  end

  def show
    url = "#{base_url}&id=#{params[:id]}"
    response = HTTP.get(url).body
    comments = Comment.where(picture_id: params[:id])
    render json: JSON.parse(response).merge(comments: comments)
  end

  private

  def base_url
    "https://pixabay.com/api/?key=#{ENV['PIXABAY_KEY']}&image_type=photo"
  end
end
