class CommentsController < ApplicationController

  def create
    p 'Params:'
    p params
    p 'Comment Params:'
    p comment_params
    comment = Comment.create(comment_params)
    if comment.valid?
      render json: comment, status: :created
    else
      render json: render_errors(comment), status: :unprocessable_entity
    end
  end

  def edit
    comment = Comment.find(params[:id])
    render json: comment
  end

  def update
    comment = Comment.find(params[:id])
    if comment.update_attributes(comment_params)
      render json: comment, status: :ok
    else
      render json: render_errors(comment), status: :unprocessable_entity
    end
  end

  def destroy
    comment = Comment.find(params[:id])
    comment.destroy
    head :no_content
  end

  private

  def comment_params
    params.require(:comment).permit(:name, :message, :picture_id)
  end

  def render_errors(note)
    { errors: note.errors }
  end
end
