Rails.application.routes.draw do
  root 'pictures#index'
  resources :pictures, only: [:show]
  resources :comments, only: [:create, :edit, :update, :destroy]
end
