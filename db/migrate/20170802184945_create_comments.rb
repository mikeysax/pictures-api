class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.string :name
      t.string :message
      t.integer :picture_id
      t.timestamps
    end
    add_index :comments, :picture_id
  end
end
